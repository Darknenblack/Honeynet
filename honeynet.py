# HoneynetSwitch
# Container dinamico
# Criacao de regras no switch
# Version: 1.1

from ryu.base import app_manager

from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls

from ryu.ofproto import ofproto_v1_3

from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import ipv6
from ryu.lib.packet import tcp
from ryu.lib.packet import udp
from ryu.lib.packet import icmp
from ryu.lib.packet import icmpv6

from paramiko import SSHClient
import threading
import paramiko
import time


class HoneynetSwitch(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(HoneynetSwitch, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.HONEYPOT_CLP = '10.0.0.2'
	self.HONEYPOT_IHM = ''
        self.GATEWAY_MAC = ''
        self.DEFAULT_CONTAINER_TEMPLATE = 'servidor'

        # Semaphore to avoid competition problems in updating the switch rules.
        self.switch_flows_semaphore = threading.Semaphore()

        self.map_port_to_container_template = Map()
        self.map_ip_to_container = Map()
        self.command_ip_list = IPList()
        self.whitelist = WhiteList()

    def init_map_container_template(self):
        print('Mapping (containers template) successful!')

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]

        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)

        datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)

        msg = ev.msg
        in_port = msg.match['in_port']
        pkt = packet.Packet(msg.data)

        # Ethernet protocol information - L2.
        pkt_ethernet = pkt.get_protocol(ethernet.ethernet)
        src_ethernet = pkt_ethernet.src
        dst_ethernet = pkt_ethernet.dst

        dpid = msg.datapath.id
        self.mac_to_port.setdefault(dpid, {})
        self.mac_to_port[dpid][src_ethernet] = in_port

        # Seeking the information of the protocols.
        if pkt.get_protocol(ipv4.ipv4):
            pkt_ip = pkt.get_protocol(ipv4.ipv4)
            ip_version = 'IPV4'
        else:
            pkt_ip = pkt.get_protocol(ipv6.ipv6)
            ip_version = 'IPV6'

        if pkt_ip:
            src_ip = pkt_ip.src
            dst_ip = pkt_ip.dst

        pkt_tcp = pkt.get_protocol(tcp.tcp)

        if pkt_tcp:
            src_port = pkt_tcp.src_port
            dst_port = pkt_tcp.dst_port

            # If bits are equal to 2, then eh is a SYN packet
            payload = pkt.protocols[-1] if pkt.protocols[-1] != pkt_tcp \
                      else ''
            self.handle_packet(msg, 'TCP', src_ethernet, dst_ethernet,
                               ip_version, src_ip, dst_ip, src_port,
                               dst_port, payload)

        else:
            pkt_udp = pkt.get_protocol(udp.udp)

            if pkt_udp:
                src_port = pkt_udp.src_port
                dst_port = pkt_udp.dst_port
                payload = pkt.protocols[-1] if pkt.protocols[-1] != pkt_udp \
                          else ''
                self.handle_packet(msg, 'UDP', src_ethernet, dst_ethernet,
                                   ip_version, src_ip, dst_ip, src_port,
                                   dst_port, payload)
            else:
                self.send_packet_out(msg)

    def send_packet_out(self, msg):
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']
        pkt = packet.Packet(msg.data)

        pkt_ethernet = pkt.get_protocol(ethernet.ethernet)
        dst = pkt_ethernet.dst

        out_port = self.get_out_port(msg, dst)
        actions = [parser.OFPActionOutput(out_port)]

        data = None

        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)
        datapath.send_msg(out)

    def get_out_port(self, msg, eth_dst):
        datapath = msg.datapath
        ofproto = datapath.ofproto
        dpid = datapath.id

        if eth_dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][eth_dst]
        else:
            out_port = ofproto.OFPP_FLOOD
        return out_port

    def handle_packet(self, msg, protocol, src_ethernet, dst_ethernet,
                      ip_version, src_ip, dst_ip, src_port, dst_port, payload):
        pkt = packet.Packet(msg.data)
        pkt_tcp = pkt.get_protocol(tcp.tcp)
        bits = pkt_tcp.bits if pkt_tcp else ''

        if dst_ip == self.HONEYPOT_IHM or self.HONEYPOT.CLP:
            if (protocol == 'TCP' and bits == 2) or protocol != 'TCP':
                self.whitelist.insert( (protocol, dst_port, src_port, src_ip) )

                out_port1 = self.get_out_port(msg, dst_ethernet)
                out_port2 = self.get_out_port(msg, src_ethernet)

                if protocol == 'TCP':
                    match1 = msg.datapath.ofproto_parser.\
                             OFPMatch(eth_type=2048, ip_proto=6,
                                      tcp_src=src_port, tcp_dst=dst_port,
                                      ipv4_src=src_ip, ipv4_dst=dst_ip)
                    match2 = msg.datapath.ofproto_parser.\
                             OFPMatch(eth_type=2048, ip_proto=6,
                                      tcp_src=dst_port, tcp_dst=src_port,
                                      ipv4_src=dst_ip, ipv4_dst=src_ip)
                else:
                    match1 = msg.datapath.ofproto_parser.\
                             OFPMatch(eth_type=2048, ip_proto=17,
                                      udp_src=src_port, udp_dst=dst_port,
                                      ipv4_src=src_ip, ipv4_dst=dst_ip)
                    match2 = msg.datapath.ofproto_parser.\
                             OFPMatch(eth_type=2048, ip_proto=17,
                                      upd_src=dst_port, udp_dst=src_port,
                                      ipv4_src=dst_ip, ipv4_dst=src_ip)

                action1 = [msg.datapath.ofproto_parser.\
                           OFPActionOutput(out_port1)]
                action2 = [msg.datapath.ofproto_parser.\
                           OFPActionOutput(out_port2)]

                # Adds the rules
                self.switch_flows_semaphore.acquire()
                self.add_flow(msg.datapath, 1, match1, action1)
                self.add_flow(msg.datapath, 1, match2, action2)
                self.switch_flows_semaphore.release()

        # Checks if part of honeypot is attack.
        if src_ip == ((self.HONEYPOT_IHM or self.HONEYPOT.CLP) and 
		       self.is_attack(protocol, src_port, dst_port, dst_ip)):
            container = self.map_ip_to_container.get(dst_ip)

            if not container:
                self.map_ip_to_container.add(dst_ip, ('-','-'))

            	if ip_version == 'IPV4':
                	new_container_name = 'net-cont-%s-%d-IP-%s'\
                	                      % (protocol, dst_port,
                	                         dst_ip.replace('.','-'))
            	else:
                	new_container_name = 'net-cont-%s-%d-IP-%s'\
                	                      % (protocol, dst_port,
                	                         dst_ip.replace(':','-'))
                	threading.Thread(target=self.create_container, \
                        	         args=(msg, src_ethernet, ip_version,
                               		       src_ip, dst_ip,
                                               new_container_name,)).start()

            else:
                return
        self.send_packet_out(msg)

    def create_container(self, msg, src_ethernet, ip_version, src_ip,
                         dst_ip, container_name):
        template = self.DEFAULT_CONTAINER_TEMPLATE
        first_time = time.time()
        ssh = SSH()

        cmd = 'lxc launch %s %s' % (template, container_name)
        ssh.exec_cmd(cmd)

        cmd = 'lxc network attach switch %s eth0' % (container_name)
        ssh.exec_cmd(cmd)

        cmd = 'lxc exec %s -- ifconfig eth0 %s' % (container_name, dst_ip)
        ssh.exec_cmd(cmd)

        cmd = 'lxc exec %s -- route add default dev eth0' % (container_name)
        ssh.exec_cmd(cmd)

        cmd = 'lxc exec %s -- cat /sys/class/net/eth0/address'\
              % (container_name)
        new_container_mac = ssh.exec_cmd(cmd).strip()

        ssh.close()

        second_time = time.time()

        # Rule creation on the switch.
        out_port1 = self.get_out_port(msg, new_container_mac)
        out_port2 = self.get_out_port(msg, src_ethernet)

        match1 = msg.datapath.ofproto_parser.\
                 OFPMatch(eth_type=2048, ipv4_src=src_ip, ipv4_dst=dst_ip)
        match2 = msg.datapath.ofproto_parser.\
                 OFPMatch(eth_type=2048, ipv4_src=dst_ip, ipv4_dst=src_ip)

        action1 = [msg.datapath.ofproto_parser.\
                  OFPActionSetField(eth_dst=new_container_mac), \
                  msg.datapath.ofproto_parser.OFPActionOutput(out_port1)]
        action2 = [msg.datapath.ofproto_parser.OFPActionOutput(out_port2)]

        self.switch_flows_semaphore.acquire()
        self.add_flow(msg.datapath, 2, match1, action1)
        self.add_flow(msg.datapath, 2, match2, action2)
        self.switch_flows_semaphore.release()

        third_time = time.time()

        print("%s criado com MAC %s em %f segundos" \
             % (container_name, new_container_mac, (second_time - first_time)))

        # New container definitions.
        container = (container_name, new_container_mac)
        self.map_ip_to_container.set(dst_ip, container)
        pkt = packet.Packet(msg.data)
        pkt[0].dst = new_container_mac
        pkt[2].csum = 0
        pkt.serialize()
        msg.data = pkt.data
        self.send_packet_out(msg)

    def is_attack(self, protocol, src_port, dst_port, dst_ip):
        if self.whitelist.in_list( (protocol, src_port, dst_port, dst_ip) ) \
                                  or self.command_ip_list.in_list(dst_ip):
            return False
        else:
            return True


class SSH:
    """Class for ssh communication with the host"""

    def __init__(self):
        self.ssh = SSHClient()
        self.ssh.load_system_host_keys()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.connect(hostname='10.0.2.1',username='asgard',password='')

    def exec_cmd(self, cmd):
        stdin,stdout,stderr = self.ssh.exec_command(cmd)
        if stderr.channel.recv_exit_status() != 0:
            return stderr.read()
        else:
            return stdout.read()

    def close(self):
        self.ssh.close()


class Map:
    """Class with mapping structure"""

    def __init__(self):
        self.my_map = {}

    def add(self, key, value):
        if not self.my_map.has_key(key):
            self.my_map[key] = value

    def remove(self, key):
        if self.my_map.has_key(key):
            del self.my_map[key]

    def get(self, key):
        if self.my_map.has_key(key):
            return self.my_map[key]
        else:
            return None

    def set(self, key, value):
        if self.my_map.has_key(key):
            self.my_map[key] = value

    def print_map(self):
        print self.my_map


class IPList:
    """Logs the IPs of Command and Control Servers of Botnets"""

    def __init__(self):
        self.list = []

        try:
            arq = open('ip_list.txt', 'r')
            lines = arq.readlines()
            for ip in lines:
                self.insert(ip.strip())
            arq.close()
            print('%d IPs of Command and Control Servers' % (self.size()))
        except:
            print('Error while reading file of Command and Control IP List')

    def size(self):
        return len(self.list)

    def in_list(self, ip):
        return ip in self.list

    def insert(self, ip):
        if not self.in_list(ip):
            self.list.append(ip)

    def remove(self, ip):
        if self.in_list(ip):
            self.list.remove(ip)


class WhiteList:
    """Class to register packages that can be released on the internet."""

    def __init__(self):
        self.list = []

    def size(self):
        return len(self.list)

    def in_list(self, item):
        return item in self.list

    def insert(self, item):
        if not self.in_list(item):
            self.list.append(item)

    def remove(self, item):
        if self.in_list(item):
            self.list.remove(item)
